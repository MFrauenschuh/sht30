/*
 * sht30.h
 *
 *  Created on: 04.12.2018
 *      Author: Martin Frauenschuh
 */

#ifndef SHT30_H_
#define SHT30_H_

#include "stm32l0xx_hal.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct {
	I2C_HandleTypeDef *i2cHandle;
	uint8_t i2cAddress;
} SHT30;

/**
 * @brief					Initialize the SHT30 temperature and relative humidity sensor
 * @param	*aSHT30			Pointer to a SHT30 struct
 * @param	*aI2cHandle		Pointer to a I2C_HandleTypeDef struct
 * @param	aI2cAddress		The I2C address of the device
 * @retval					If any errors occurred.
 */
bool sht30_init(SHT30 *aSHT30, I2C_HandleTypeDef *aI2cHandle,
		uint8_t aI2cAddress);

/**
 * @brief				Read data from the SHT30 sensor
 * @param	*aSHT30		Pointer to a SHT30 struct
 * @param	*aTemp		Call by Reference for temperature data
 * @param	*aHumidity	Call by Reference for humidity data
 * @retval				If any errors occurred.
 */
bool sht30_read(SHT30 *aSHT30, float *aTemp, float *aHumidity);

/**
 * @brief						Read data from the SHT30 sensor and convert it to a string
 * @param	*aSHT30				Pointer to a SHT30 struct
 * @param	*aTempString		The converted data will be written to the location
 * 								where the pointer is pointing. (Data format: +xxx.x\0) 7 bytes will be written
 * @param	*aHumidityString	The converted data will be written to the location
 * 								where the pointer is pointing. (Data format: xx.x\0) 5 bytes will be written
 * @retval						If any errors occurred.
 */
bool sht30_readString(SHT30 *aSHT30, char *aTempString, char *aHumidityString);

#endif /* SHT30_H_ */
