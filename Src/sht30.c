/*
 * sht30.c
 *
 *  Created on: 04.12.2018
 *      Author: Martin Frauenschuh
 */

#include <sht30.h>

bool sht30_init(SHT30 *aSHT30, I2C_HandleTypeDef *aI2cHandle,
		uint8_t aI2cAddress) {
	aSHT30->i2cHandle = aI2cHandle;
	aSHT30->i2cAddress = aI2cAddress;

	return true;
}

bool sht30_read(SHT30 *aSHT30, float *aTemp, float *aHumidity) {

	uint8_t txData[2], rxData[6];
	HAL_StatusTypeDef txStatus, rxStatus;

	txData[0] = 0x2C;
	txData[1] = 0x06;

	txStatus = HAL_I2C_Master_Transmit(aSHT30->i2cHandle,
			aSHT30->i2cAddress << 1, txData, 2, 100);

	if (txStatus == HAL_OK) {
		rxStatus = HAL_I2C_Master_Receive(aSHT30->i2cHandle,
				aSHT30->i2cAddress << 1, rxData, 6, 100);
		if (rxStatus == HAL_OK) {
			*aTemp = ((((rxData[0] << 8) | rxData[1]) * 175) / 65535.0) - 45;
			*aHumidity = ((((rxData[3] << 8) | rxData[4]) * 100) / 65535.0);
			return true;
		}
	}
	return false;
}

bool sht30_readString(SHT30 *aSHT30, char *aTempString, char *aHumidityString) {
	float temp, humidity;

	if (sht30_read(aSHT30, &temp, &humidity)) {
		sprintf(aTempString, "%+3.1f", temp);
		sprintf(aHumidityString, "%2.1f", humidity);
		return true;
	} else
		return false;
}

